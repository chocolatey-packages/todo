### Convert md5/sha1 to sha256
|Count|Folder|
|-----|------|
|1|crystaldiskmark|
|2|datahealthcheck|
|2|dotpeek|
|1|duplicatecommander|
|1|easytag|
|1|encryptr|
|1|feathercoin|
|1|findthatfont|
|1|fontmatrix|
|1|fsum|
|1|fsumfrontend|
|1|fv.install|
|1|fv.portable|
|1|gamedownloader.install|
|1|gamedownloader.portable|
|1|gtk-runtime|
|2|gurtle|
|1|hashdeep|
|1|hexed|
|1|hmne|
|2|hub|
|1|imageglass|
|1|isoplex|
|2|italc|
|1|izpack|
|1|jstock|
|1|keepass-plugin-1p2kp|
|1|keepass-plugin-chromeipass|
|1|keepass-plugin-cw6import|
|1|keepass-plugin-ewalletdatalib|
|1|keepass-plugin-gost|
|1|keepass-plugin-kpcli|
|1|keepass-plugin-passafari|
|1|keepass-plugin-passifox|
|1|keepass-plugin-qualityhighlighter|
|1|komodo-edit|
|1|kss|
|1|librecrypt|
|1|lili.install|
|1|lotusnotes-addin-notesipass|
|2|lsasecretsdump|
|2|md5summer|
|3|md5sums|
|1|mooncoin|
|2|movieexplorer|
|1|msigna|
|1|mtn|
|2|netpass.portable|
|2|nircmd2|
|1|npackd-cli.install|
|1|npackd-cli.portable|
|2|npackd.install|
|2|npackd.portable|
|1|pandabank.install|
|1|pandabank.portable|
|2|passwordfox|
|1|pendmoves|
|2|piratebrowser|
|1|procexp|
|1|procmon|
|2|pulse|
|1|py2exe|
|2|quantumdreams|
|2|quantumminds|
|2|quantumshooter|
|1|sabnzbd|
|1|sardu|
|2|sniffpass.portable|
|1|superorca|
|1|sylpheed|
|2|tabspace|
|2|twisted|
|1|udpixel.install|
|1|udpixel.portable|
|1|usagestats|
|2|vcredist2005|
|1|veracrypt|
|1|windowdetective|
|2|wirelesskeyview|
|1|wxhexeditor|
|1|x64dbg|
|1|xiphqt|
|1|xonotic|
|1|zsnes|
### Catch to remove
|Count|Folder|
|-----|------|
|1|bitmessage|
|1|faviconizer|
|1|flac|
|1|ftpdmin|
|1|hte|
|1|izpack|
|1|jhead|
|2|keepass-plugin-1p2kp|
|2|keepass-plugin-autotypecustomfieldpicker|
|2|keepass-plugin-chromeipass|
|2|keepass-plugin-cw3import|
|2|keepass-plugin-cw6import|
|2|keepass-plugin-databasebackup|
|2|keepass-plugin-ewalletdatalib|
|2|keepass-plugin-fieldsadminconsole|
|2|keepass-plugin-gost|
|2|keepass-plugin-ioprotocolext|
|2|keepass-plugin-itanmaster|
|2|keepass-plugin-keecloud|
|2|keepass-plugin-keeotp|
|2|keepass-plugin-kpcli|
|2|keepass-plugin-mskeyimporter|
|2|keepass-plugin-osk|
|2|keepass-plugin-passafari|
|2|keepass-plugin-passifox|
|2|keepass-plugin-qrcodegen|
|2|keepass-plugin-qualitycolumn|
|2|keepass-plugin-rdp|
|2|keepass-plugin-spmimport|
|2|keepass-plugin-trayrecent|
|2|keepass-plugin-traytotp|
|2|keepass-plugin-truecryptmount|
|2|keepass-plugin-twofishcipher|
|2|keepass-plugin-winkee|
|1|kvrt|
|1|lotusnotes-addin-notesipass|
|1|mooncoin|
|1|peerunity.install|
|1|peerunity.portable|
|1|regexpeditor|
|1|sabnzbd|
|1|sendmessage|
|1|soluto|
|1|sre|
|1|sylpheed|
|1|windows-tweaker.portable|
|1|znotes|
